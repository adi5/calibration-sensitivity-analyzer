import io
import os
import urllib.request

import matplotlib.pyplot as plt
plt.style.use('ggplot')
import matplotlib.image as mpimg
from matplotlib.backends.backend_pdf import PdfPages

import numpy as np
import pandas as pd

from textwrap import wrap

from scipy.interpolate import *
from scipy.stats import *
from tkinter import *
from tkinter import messagebox
import tempfile
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
from IPython.display import HTML
import plotly
from plotly.offline import download_plotlyjs, init_notebook_mode, iplot
from plotly.graph_objs import *
# from calibration_analytics import *
# import calibration_analytics
from plotly import tools
from tkinter import filedialog
import xlsxwriter 

pd.options.mode.chained_assignment = None 


 #Used to import a raw CHI txt file and return a pandas dataframe of time, electrode current values, and statistics across axis=1
def import_CHI_file(filepath, delimeter = ','):
    possible_col_headers = ['time','Time','potential','Potential','current','Current','charge','Charge']

    
    #Find the column header line
    text_file = open(filepath, 'r')
    lines_reversed = reversed(text_file.readlines())    
    line_num_reversed = 0
    is_first_instance = False           
    for line in lines_reversed:
        if line not in ['\n', '\r\n']:
            line_num_reversed += 1
        if is_first_instance == False and any((header in line) for header in possible_col_headers):
            data_header_line_num_reversed = line_num_reversed
            is_first_instance = True
    text_file.close()

    #Find the total number of lines in file
    data_header_line_num = line_num_reversed - data_header_line_num_reversed    
    
    #Read into a Pandas DF
    raw_CHI_df = pd.read_csv(filepath,sep=delimeter,header=data_header_line_num, parse_dates=[0])   
    
    if 'Time/s' in raw_CHI_df:
        
        raw_CHI_df['Time/s']=raw_CHI_df['Time/s'].astype(float)

        raw_CHI_df['AVG'] = raw_CHI_df.loc[:,' i1/A':].mean(axis=1)
        raw_CHI_df['STD'] = raw_CHI_df.loc[:,' i1/A':].std(axis=1)
        raw_CHI_df['RSD']=(raw_CHI_df['STD']/raw_CHI_df['AVG'])*100
        raw_CHI_df.rename(columns={' i1/A':1,' i2/A':2,' i3/A':3,' i4/A':4,' i5/A':5,' i6/A':6,' i7/A':7,' i8/A':8}, inplace=True)
    elif 'Time/sec' in raw_CHI_df:
        raw_CHI_df['Time/s']=raw_CHI_df['Time/sec'].astype(float)
        raw_CHI_df.drop(['Time/sec'],axis=1, inplace=True)
        raw_CHI_df['AVG'] = raw_CHI_df.loc[:,' i1/A':].mean(axis=1)
        raw_CHI_df['STD'] = raw_CHI_df.loc[:,' i1/A':].std(axis=1)
        raw_CHI_df['RSD']=(raw_CHI_df['STD']/raw_CHI_df['AVG'])*100
        raw_CHI_df.rename(columns={' i1/A':1,' i2/A':2,' i3/A':3,' i4/A':4,' i5/A':5,' i6/A':6,' i7/A':7,' i8/A':8}, inplace=True)

    return raw_CHI_df

#Used to create a dataframe of concentration steps, corresponding time points and average electrode current
def calibration_curve_df(raw_calibration_df, time_list, conc_list):
    # Make a new dataframes using rows that match the user inputed time steps
    time_df = raw_calibration_df[raw_calibration_df['Time/s'].isin(time_list)]
    # time_conc_df = time_df[['Time/s','AVG','STD','RSD',1,2,3,4,5,6,7,8]]
    time_conc_df = time_df
    time_conc_df['conc'] = conc_list

    return time_conc_df


#Used to iterate through CHI .txt files in a given folder and create a dictionary of time, AVG and conc dataframes
def create_analysis_dict(path):
    global raw_CHI_dict
    raw_CHI_dict = {}
    global analysis_dict 
    analysis_dict = {}

    for file in os.listdir(path):
        if file.endswith(".txt"):
            name = os.path.splitext(file)[0]
            # print(name)
            # print(os.path.join(path,file))
            raw_CHI_dict[name] = import_CHI_file(filepath = (os.path.join(path,file)))
            analysis_dict[name] = calibration_curve_df(raw_CHI_df=raw_CHI_dict[name],time_list=time_list, conc_list=conc_list)
            # print(raw_CHI_dict.keys())
            # print(analysis_dict.keys())

def drop_outliers(raw_calibration_df):
    #Function to return the magnitude of a float
    def magnitude(x):
        return int(math.floor(math.log10(x)))
    #Import only the electrodes
    imported_df_electrodes=raw_calibration_df.drop(['Time/s','AVG','STD','RSD'], axis=1)
    #Make a list of the standard deviation of each electrode
    std_list = []
    for column in imported_df_electrodes:
#         print(magnitude(imported_df_electrodes[column].std()))
        std_list.append(abs(magnitude(imported_df_electrodes[column].std())))
    std_list=pd.Series(std_list)
    #Take the mean of the list of electrode standard deviations. 
    #Drop a column if its SD is more than 1.75 standard deviations than the mean of the list of electrode SDs
    for i in enumerate(std_list):
        if i[1] > (1.75*std_list.std())+std_list.mean():
            std_list_dropped = std_list.loc[std_list.values != i[1] ]
        elif i[1] > (1.75*std_list.std())+std_list.mean():
            std_list_dropped = std_list.loc[std_list.values != i[1] ]
   #Create a list of strings corresponding to the electrodes to drop
    drop_list= list(std_list_dropped.index)
    drop_list =[(int(item)+1) for item in drop_list ]
    drop_list_int=drop_list
    drop_list = [str(item) for item in drop_list]
    drop_list=list(map(int,drop_list))

    #Compare dropped and normal lists
    regular_list = list(std_list.index + 1)
    print ("Column {} was dropped".format(list(set(regular_list).difference(set(drop_list_int)))))
    dropped_column = list(set(regular_list).difference(set(drop_list_int)))
    #Return the electrode # that was dropped and the new DF, wherein that electrode has been dropped
    cleaned_df = raw_calibration_df[drop_list]
    cleaned_df['AVG'] = cleaned_df.mean(axis=1)
    cleaned_df['STD'] = cleaned_df.std(axis=1)
    cleaned_df['RSD'] = (cleaned_df['STD']/cleaned_df['AVG'])*100
    cleaned_df['Time/s']=raw_calibration_df['Time/s']
    
    return dropped_column,cleaned_df



    


def show_entry_fields():
   # print("Time List: %s\nConc List: %s" % (e1.get().split(), e2.get().split()))

   global time_list
   global conc_list
   global conc_list
   global save_file_name
   time_list = e1.get().split(',')
   conc_list = e2.get().split(',')
   save_file_name = e3.get()
   length_lists = [len(time_list), len(conc_list)]
   if len(set(length_lists)) != 1:
    print("Lists are not the same length")
    messagebox.showinfo("Warning", "Lists are not the same length. Please re-enter the time and concentration values ")

   print(save_file_name)

   conc_list = pd.Series(conc_list)
   print(time_list)
   print(conc_list)
    #Return a list for the time and corresponding concentrations 
   return time_list
   return conc_list
   return save_file_name

   e1.delete(0,END)
   e2.delete(0,END)
   e3.delete(0,END)

def browse_button():
    global file_path
    
    file_path = filedialog.askopenfilename()
    

def browse_save_directory_button():
    global save_directory_path
    save_directory_path = filedialog.askdirectory()

    

def instructions_button():
    messagebox.showinfo("Instructions", "1. Enter the time list, separated by commas. e.g. 600,1200,1800 \n 2. Enter the concentration lists, separated by commas. e.g. 10,15,25 \n 3. Enter the File Name prefix \n 4. Select the folder containing the data \n 5. Press Complete entering time,concentration and save filename \n 6. Press Continue")


    # Directory button and label

master = Tk()
master.title("Biolinq Calibration Data Analyzer")

image = PhotoImage(file="biolinq_logo.png")

#Labels
Label(master,image=image).grid(row=0)
Label(master, text="Time List").grid(row=4, column=0, sticky=W, pady=4)
Label(master, text="Concentration List").grid(row=5, column=0, sticky=W, pady=4)
Label(master, text="Sensor Name").grid(row=3, column=0, sticky=W, pady=4)

e1 = Entry(master, width=30)
e2 = Entry(master, width=30)
e3 = Entry(master, width=30)

    #Default text for fields
# e1.insert(10,"360,750,1060,1380,1680,2000,2321")
e1.insert(10,"600,1200,1800,2400,3000,3600,4200")
e2.insert(10,"0,2,6,10,14,18,22")
    # e2.insert(10,"Jill")
#Entry fields
e1.grid(row=4, column=1)
e2.grid(row=5, column=1)
e3.grid(row=3, column =1)


#Buttons
Button(master, text="Instructions", command=instructions_button).grid(row=1, column=0, sticky=W, pady=4)
Button(master, text='Complete entering time,concentration and save file name', command=show_entry_fields).grid(row=6, column=0, sticky=W, pady=4)
Button(master, text="Browse for Data File", command=browse_button).grid(row=2, column=0, sticky=W, pady=4)
# Button(master, text="Browse for Directory to Save Output Files", command=browse_save_directory_button).grid(row=7, column=0, sticky=W, pady=4)
Button(master, text='Continue', command=master.quit).grid(row=8, column=0, sticky=W, pady=4)

mainloop( )

print(file_path)
print(save_file_name)
# print(save_directory_path)

time_list = list(map(int,time_list))
conc_list = list(map(int,conc_list))
raw_calibration_df = import_CHI_file(filepath=file_path)
processed_calibration_df=calibration_curve_df(raw_calibration_df, time_list, conc_list)
try:
    dropped_column,raw_calibration_df_no_outliers=(drop_outliers(raw_calibration_df))
except:
    print("Error in outlier detection")
try:
    processed_calibration_df_no_outliers=calibration_curve_df(raw_calibration_df_no_outliers, time_list, conc_list)

except:
    print("Error in making a calibration curve from outlier dataset")


    #create pandas excel writer with xlsxwriter as the engine
writer = pd.ExcelWriter(save_file_name + ' Output.xlsx', engine='xlsxwriter')
raw_calibration_df.to_excel(writer, sheet_name = "Raw Data")
processed_calibration_df.to_excel(writer, sheet_name="Processed Data")
try:
    raw_calibration_df_no_outliers.to_excel(writer, sheet_name='Raw Data no outliers')
    processed_calibration_df_no_outliers.to_excel(writer, sheet_name="Processed Data no outliers")
except:
    print("Error writing outlier dropped dataset to excel")


workbook  = writer.book
worksheet = writer.sheets["Processed Data"]

#Calibration Curve Chart
chart = workbook.add_chart({'type': 'scatter'})
chart.add_series({
    'values': '=Processed Data!$C$2:$C$15',
    'categories':     '=Processed Data!$N$2:$N$15',
    'y_error_bars': {'type': 'custom', 'plus_values' :'=Processed Data!$D$2:$D$15','minus_values' :'=Processed Data!$D$2:$D$15'},
    'gap':        2,
    'trendline': {
        'type': 'polynomial',
        'order': 2,
        'display_equation': True,
        },
})

# Configure the chart title and axes
chart.set_title({'name': 'Calibration Curve'})
chart.set_x_axis({'name': 'Conc', 'position_axis': 'on_tick'})
chart.set_y_axis({'name': 'AVG Current', 'major_gridlines': {'visible': False}})

# Insert the chart into the worksheet.
chart.set_style(10)
worksheet.insert_chart('A20', chart)

worksheet2 = writer.sheets["Raw Data"]
chart2 = workbook.add_chart({'type': 'scatter'})
#Electrode 1
chart2.add_series({
    'values': '=Raw Data!$C$2:$C$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})

#Electrode 2
chart2.add_series({
    'values': '=Raw Data!$D$2:$D$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})

#Electrode 3
chart2.add_series({
    'values': '=Raw Data!$E$2:$E$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})

#Electrode 4
chart2.add_series({
    'values': '=Raw Data!$F$2:$F$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})

#Electrode 5
chart2.add_series({
    'values': '=Raw Data!$G$2:$G$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})

#Electrode 6
chart2.add_series({
    'values': '=Raw Data!$H$2:$H$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})

#Electrode 7
chart2.add_series({
    'values': '=Raw Data!$I$2:$I$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})

#Electrode 8
chart2.add_series({
    'values': '=Raw Data!$J$2:$J$4000',
    'categories':     '=Raw Data!$B$2:$B$4000',
    'gap':        2,
})


# Configure the chart title and axes
chart2.set_title({'name': 'Raw Calibration Data'})
chart2.set_x_axis({'name': 'Time [s]', 'position_axis': 'on_tick'})
chart2.set_y_axis({'name': 'Electrode Current [A]', 'major_gridlines': {'visible': False}})

# Insert the chart into the worksheet.
chart2.set_style(10)

worksheet2.insert_chart('O2', chart2)


# Close the Pandas Excel writer and output the Excel file.
writer.save()



