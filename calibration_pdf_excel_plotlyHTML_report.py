import io
import os
import urllib.request

import matplotlib.pyplot as plt
plt.style.use('ggplot')
import matplotlib.image as mpimg
from matplotlib.backends.backend_pdf import PdfPages

import numpy as np
import pandas as pd

from textwrap import wrap

from scipy.interpolate import *
from scipy.stats import *
from tkinter import *
from tkinter import messagebox
import tempfile
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
from IPython.display import HTML
import plotly
from plotly.graph_objs import Scatter,Layout
from calibration_analytics import *

import xlsxwriter 

pd.options.mode.chained_assignment = None 


 #Used to import a raw CHI txt file and return a pandas dataframe of time, electrode current values, and statistics across axis=1
def import_CHI_file(filepath, delimeter = ','):
        possible_col_headers = ['time','Time','potential','Potential','current','Current','charge','Charge']

        
        #Find the column header line
        text_file = open(filepath, 'r')
        lines_reversed = reversed(text_file.readlines())    
        line_num_reversed = 0
        is_first_instance = False           
        for line in lines_reversed:
            if line not in ['\n', '\r\n']:
                line_num_reversed += 1
            if is_first_instance == False and any((header in line) for header in possible_col_headers):
                data_header_line_num_reversed = line_num_reversed
                is_first_instance = True
        text_file.close()

        #Find the total number of lines in file
        data_header_line_num = line_num_reversed - data_header_line_num_reversed    
        column_names = ['time','1','2','3','4','5','6','7','8']

        #Read into a Pandas DF
        raw_CHI_df = pd.read_csv(filepath,sep=delimeter,header=data_header_line_num, parse_dates=[0], names=column_names)   

        raw_CHI_df.time = raw_CHI_df.time.astype(float)

        #Calculate the following statistics and add to the DataFrame
        raw_CHI_df['AVG']=raw_CHI_df[['1','2','3','4','5','6','7','8']].mean(axis=1)
        raw_CHI_df['STD']=raw_CHI_df[['1','2','3','4','5','6','7','8']].std(axis=1)
        raw_CHI_df['RSD']=(raw_CHI_df['STD']/raw_CHI_df['AVG'])*100
        raw_CHI_df['RSD']=(raw_CHI_df['STD']/raw_CHI_df['AVG'])*100

        return raw_CHI_df

#Used to create a dataframe of concentration steps, corresponding time points and average electrode current
def calibration_curve_df(raw_CHI_df, time_list, conc_values):
    # Make a new dataframes using rows that match the user inputed time steps
    time_df = raw_CHI_df[raw_CHI_df['time'].isin(time_list)]
    time_conc_df = time_df[['time','AVG','STD']]
    time_conc_df['conc'] = conc_values.values

    return time_conc_df

#Used to iterate through CHI .txt files in a given folder and create a dictionary of time, AVG and conc dataframes
def create_analysis_dict(path):
    global raw_CHI_dict
    raw_CHI_dict = {}
    global analysis_dict 
    analysis_dict = {}

    for file in os.listdir(path):
        if file.endswith(".txt"):
            name = os.path.splitext(file)[0]
            # print(name)
            # print(os.path.join(path,file))
            raw_CHI_dict[name] = import_CHI_file(filepath = (os.path.join(path,file)))
            analysis_dict[name] = calibration_curve_df(raw_CHI_df=raw_CHI_dict[name],time_list=time_list, conc_values=conc_values)
            # print(raw_CHI_dict.keys())
            # print(analysis_dict.keys())
    

#Tkinter GUI code to accept a time and concentration vector (for calibration curve generation) 
# and specify a folder location containing raw CHI txt files
def show_entry_fields():
   print("Time List: %s\nConc List: %s" % (e1.get().split(), e2.get().split()))

   global time_list
   global conc_list
   global conc_values
   global save_file_name
   time_list = e1.get().split(',')
   conc_list = e2.get().split(',')
   save_file_name = e3.get()
   length_lists = [len(time_list), len(conc_list)]
   if len(set(length_lists)) != 1:
    print("Lists are not the same length")
    messagebox.showinfo("Warning", "Lists are not the same length. Please re-enter the time and concentration values ")

   print(save_file_name)

   conc_values = pd.Series(conc_list)
    #Return a list for the time and corresponding concentrations 
   return time_list
   return conc_list
   return save_file_name

   e1.delete(0,END)
   e2.delete(0,END)
   e3.delete(0,END)

def browse_button():
    global folder_path
    folder_path = filedialog.askdirectory()
    print(folder_path)

def instructions_button():
    messagebox.showinfo("Instructions", "1. Enter the time list, separated by commas. e.g. 600,1200,1800 \n 2. Enter the concentration lists, separated by commas. e.g. 10,15,25 \n 3. Enter the File Name prefix \n 4. Select the folder containing the data \n 5. Press Complete entering time,conentration and save filename \n 6. Press Continue")


    # Directory button and label

master = Tk()
master.title("Biolinq Calibration Data Analyzer")

image = PhotoImage(file="biolinq_logo.png")

#Labels
Label(master,image=image).grid(row=0)
Label(master, text="Time List").grid(row=2, column=0, sticky=W, pady=4)
Label(master, text="Conentration List").grid(row=3, column=0, sticky=W, pady=4)
Label(master, text="Save File name").grid(row=4, column=0, sticky=W, pady=4)

e1 = Entry(master)
e2 = Entry(master)
e3 = Entry(master)

    #Default text for fields
    # e1.insert(10,"Miller")
    # e2.insert(10,"Jill")
#Entry fields
e1.grid(row=2, column=1)
e2.grid(row=3, column=1)
e3.grid(row=4, column =1)

#Buttons
Button(master, text="Instructions", command=instructions_button).grid(row=1, column=0, sticky=W, pady=4)
Button(master, text='Continue', command=master.quit).grid(row=7, column=0, sticky=W, pady=4)
Button(master, text='Complete entering time,concentration and save file name', command=show_entry_fields).grid(row=6, column=0, sticky=W, pady=4)
Button(master, text="Browse for Data Output Folder", command=browse_button).grid(row=5, column=0, sticky=W, pady=4)


mainloop( )

print(folder_path)
print(save_file_name)


#Try to loop through a folder, and import all files + run analytics
# path =r'/Users/adityamookerjee/Documents/Code/Calibration Analysis/Sample Data' 

#Run create_analysis_dict on the given path, outputting a raw and process dict of dataframes
create_analysis_dict(path=folder_path)
raw_CHI_dict=calibration_analytics.create_raw_CHI_dict(path=folder_path)
calibration_dict=calibration_analytics.create_calibration_dict(path=folder_path, time_list=time_list, conc_values=conc_values)
LOQ=calibration_analytics.calc_LOQ(calibration_dict, time_list, raw_CHI_dict)
LOD=calibration_analytics.calc_LOD(calibration_dict, time_list, raw_CHI_dict)
LO_df=pd.DataFrame.from_dict([LOD, LOQ]).transpose()
LO_df.columns = ['LOD [mM]','LOQ [mM]']
#Create PDF Report


#Biolinq logo
img=mpimg.imread('biolinq_logo.png')

#Page 1 Summary of Electrode Currents
sensor_list=['1','2','3','4','5','6','7','8','AVG']


#Generate a PDF report containg plots, tables, etc
with PdfPages(save_file_name + ' Sensor analytics output.pdf') as pdf:
    #Cover Page
    plt.figure() 
    plt.axis('off')
    plt.text(0.5,0.5,"Calibration Analytics",ha='center',va='center')
    plt.imshow(img)
    pdf.savefig()
    plt.close() 
    
    
    
    sensor_list=['1','2','3','4','5','6','7','8','AVG']

    #Sensor plots for all electrodes
    fig,ax = plt.subplots()
    
    for key in raw_CHI_dict.keys():
        current_time_plot=raw_CHI_dict[key].plot(x='time', y=sensor_list, title="%s Sensor Plot for all Electrodes " %key)
        current_time_plot.set_xlabel("time [s]")
        current_time_plot.set_ylabel("Sensor Value [A]")
        pdf.savefig()
        plt.close()
        
        
    #Calibration curves, all three sensors 
    fig,ax = plt.subplots()
    
    polyfit_linear_list=[]
    iterate = -1
    pfit = 0

    for key in analysis_dict.keys():
        iterate += 1
        plt.figure(iterate)

        x=pd.to_numeric((analysis_dict[key]['conc']))
        y=pd.to_numeric((analysis_dict[key]['AVG']))
        #Calculate parameters for 2nd order polynomial fit
        p2 = np.polyfit(x,y,2)
        slope, intercept, r_value, p_value, std_err = linregress(x,y)
        #Save a list of the linear region coefficients of the 2nd order polynomial fit
        polyfit_linear_list.append(p2[1])
        #Set labels for axes
        plt.xlabel("CONC [mM]")
        plt.ylabel("AVG ELECTRODE CURRENT [A]")


        #Set Plot title
        plt.suptitle(key)
        plt.title("R^2 value: %s" %pow(r_value,2))
        plt.plot(x,y,'o')
        plt.plot(x,np.polyval(p2,x),'r-')
        plt.errorbar(x,np.polyval(p2,x),yerr=analysis_dict[key]['STD'])
        
        pdf.savefig()
        plt.close()
    
        

    #Create a figure with three subplots
    graph=plt.figure()
    graph, ax = plt.subplots(len(analysis_dict), 1, figsize=(7,5), sharex=True)
    
    #Save Parameters 
    polyfit_linear_parameters=[]
    r2_parameters=[]
    sensor_list=[]


    for a,key in zip(ax,analysis_dict.keys() ):
        x=pd.to_numeric((analysis_dict[key]['conc']))
        y=pd.to_numeric((analysis_dict[key]['AVG']))
         #Calculate parameters for 2nd order polynomial fit
        p2 = np.polyfit(x,y,2)



        slope, intercept, r_value, p_value, std_err = linregress(x,y)

        #Save a list of the linear region coefficients of the 2nd order polynomial fit + R2 values
        polyfit_linear_parameters.append(p2[1])
        r2_parameters.append(pow(r_value,2))
        sensor_list.append(key)

        #Make a Dataframe of Parameters
        parameters_df = pd.DataFrame(
        {'sensor':sensor_list,'Sensitivity [nA/mM]':polyfit_linear_parameters,
        'R2 Parameter':r2_parameters}, index=None)

        a.plot(x,y,'o', label='Original Data')
        a.plot(x,np.polyval(p2,x),'r-', label='Fitted Curve')
        a.errorbar(x,np.polyval(p2,x),yerr=analysis_dict[key]['STD'])
        a.set_xlabel('%s Concentration [mM]'%key)
        a.set_ylabel(wrap('Avg Current [A]'))

        plt.legend(loc='upper left')
        plt.tight_layout() 

    
    pdf.savefig()
    plt.close()

            
    #Summary table for fit parameters
    fig, ax = plt.subplots()
      # Hide axes
    ax.xaxis.set_visible(False) 
    ax.yaxis.set_visible(False)
    ax.table(cellText=parameters_df.values, colLabels=parameters_df.columns, loc='center')
    pdf.savefig()
    plt.close()

    #Summary table for LOD,LOQ
    fig, ax = plt.subplots()
     # Hide axes
    ax.xaxis.set_visible(False) 
    ax.yaxis.set_visible(False)
    ax.table(cellText=LO_df.values, colLabels=LO_df.columns, loc='center')
    pdf.savefig()
    plt.close()



    
    #create pandas excel writer with xlsxwriter as the engine
writer = pd.ExcelWriter(save_file_name + ' Sensor analytics output.pdf.xlsx', engine='xlsxwriter')


    #Loop through the raw dictionary and output a sheet for each sensor
for key in raw_CHI_dict:
    raw_CHI_dict[key].to_excel(writer, sheet_name = key + " raw data")
    
for key in analysis_dict:
    analysis_dict[key].to_excel(writer, sheet_name = key + " calibration curve data")

    #close writer and output excel file
writer.save()







#Generate a Plotly based HTML Dashboard
    #Define trace for raw I-T Data
    #Create Multiple traces for each sensor

raw_CHI_data = []
for key in raw_CHI_dict.keys():
    trace = Scatter(x=raw_CHI_dict[key]['time'], 
                   y=raw_CHI_dict[key]['1'],
                   mode='markers',
                   showlegend=True, name=key)
    raw_CHI_data.append(trace)

raw_output_plot=plotly.offline.plot({
    "data" : raw_CHI_data,
    "layout" : Layout(title="<b> Raw Plot for all sensors </b>",
                     xaxis=dict(
                     title='<b>Time [s]</b>',
                     zeroline = False,
                     gridcolor = 'rgb(183,183,183)',
                     showline=True
                     ),
                     yaxis=dict(
                     title='<b>Current [A]</b>',
                     gridcolor='rgb(183,183,183)',
                     zeroline=False
                     ))
},filename='raw_output.html', auto_open=False)

    #Define trace for calibration curve data
    #Create Multiple traces for each sensor
reg_data = []
fit_data = []
polyfit_linear_parameters = []
r2_parameters = []
sensor_list = []
for key in analysis_dict.keys():
    num_x = pd.to_numeric((analysis_dict[key]['conc']))
    num_y = pd.to_numeric((analysis_dict[key]['AVG']))
    p2 = np.polyfit(num_x,num_y,2)

    poly_trace = Scatter(x=num_x,y=np.polyval(p2,num_x),error_y=dict(type='percent',value=analysis_dict[key]['STD'], visible=True),
                        mode='lines', 
                        showlegend=True, 
                        name=key+" polynomial fit")
    
    reg_trace = Scatter(x=analysis_dict[key]['conc'], 
                   y=analysis_dict[key]['AVG'],
                   mode='markers',
                   showlegend=True, 
                   name=key)
    #Calculate Parameters for 2nd order polyfit
    p2 = np.polyfit(num_x,num_y,2)
    slope, intercept, r_value, p_value, std_err = linregress(num_x,num_y)
    
    #Save parameters
    polyfit_linear_parameters.append(p2[1])
    r2_parameters.append(pow(r_value,2))
    sensor_list.append(key)
    parameters_df = pd.DataFrame({'sensor':sensor_list,'Sensitivity [nA/mM]' : polyfit_linear_parameters, 'R^2 parameter' : r2_parameters})
    fit_data.append(poly_trace)
    reg_data.append(reg_trace)
    
    
datas=reg_data+fit_data
calibration_curve_plot=plotly.offline.plot({
    "data" : datas,
    "layout" : Layout(title="<b> Calibration Curves for All sensors </b>",
                     xaxis=dict(
                     title='<b>Concentration [mM]</b>',
                     zeroline = False,
                     gridcolor = 'rgb(183,183,183)',
                     showline=True
                     ),
                     yaxis=dict(
                     title='<b>Average Current across all electrodes [A]</b>',
                     gridcolor='rgb(183,183,183)',
                     zeroline=False
                     ))
},filename='calibration_curves.html', auto_open=False)

    #Create Summary Table of Polyfit parameters
summary_table_1 = parameters_df
summary_table_1 = summary_table_1\
    .to_html()\
    .replace('<table border="1" class="dataframe">','<table class="table table-striped">') # use bootstrap styling

summary_table_2 = LO_df
summary_table_2 = summary_table_2\
    .to_html()\
    .replace('<table border="1" class="dataframe">','<table class="table table-striped">') # use bootstrap styling

html_string = '''
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <style>body{ margin:0 100; background:whitesmoke; }</style>
    </head>
    <body>
        <img src="http://biolinq.me/wp-content/themes/biolinq/images/logo@2x.png" alt="logo" />
        <h1>Calibration Dashboard</h1>

        <!-- *** Section 1 *** --->
        <h2>Raw Sensor Output Plot</h2>
        <iframe width="1000" height="550" frameborder="0" seamless="seamless" scrolling="no" \
src="''' + raw_output_plot + '''"></iframe>
        <p>I-T plot for electrodes for all sensors</p>
        
        <!-- *** Section 2 *** --->
        <h2>Calibration Curve Plot</h2>
        <iframe width="1000" height="1000" frameborder="0" seamless="seamless" scrolling="no" \
src="''' + calibration_curve_plot + '''"></iframe>
        <p>Calibration Curve Plot for All sensors</p>
        <h3>Polynomial fit parameters for calibration Curves</h3>
        ''' + summary_table_1 + '''
        <h3>LOD and LOQ for Sensors</h3>
        ''' + summary_table_2 + '''

    </body>
</html>'''

    #Specify where to save the generated HTML file 
f = open('/Users/adityamookerjee/calibration-dashboard/' + save_file_name + ' report.html','w')
f.write(html_string)
f.close()



    
    



