
import matplotlib.ticker  
import numpy as np 
import pylab
import os
import matplotlib.pyplot as plt
import pandas as pd
from scipy.spatial import cKDTree 
from collections import OrderedDict
import math
from datetime import datetime
import glob

import seaborn as sns
from scipy.interpolate import *
from scipy.stats import *
from IPython.display import HTML

pd.options.mode.chained_assignment = None



class calibration_analytics:

	def __init__(self):
		self.possible_col_headers = []
		self.column_names = []
		self.time_list = []
		self.conc_list = []

	#Used to import a raw CHI txt file and return a pandas dataframe of time, electrode current values, and statistics across axis=1
	# def import_CHI_file(filepath, delimeter = ','):
	# 	possible_col_headers = ['time','Time','potential','Potential','current','Current','charge','Charge']

		
	# 	#Find the column header line
	# 	text_file = open(filepath, 'r')
	# 	lines_reversed = reversed(text_file.readlines())	
	# 	line_num_reversed = 0
	# 	is_first_instance = False			
	# 	for line in lines_reversed:
	# 		if line not in ['\n', '\r\n']:
	# 			line_num_reversed += 1
	# 		if is_first_instance == False and any((header in line) for header in possible_col_headers):
	# 			data_header_line_num_reversed = line_num_reversed
	# 			is_first_instance = True
	# 	text_file.close()

	# 	#Find the total number of lines in file
	# 	data_header_line_num = line_num_reversed - data_header_line_num_reversed	
	# 	column_names = ['time','1','2','3','4','5','6','7','8']

	# 	#Read into a Pandas DF
	# 	raw_CHI_df = pd.read_csv(filepath,sep=delimeter,header=data_header_line_num, parse_dates=[0], names=column_names)	

	# 	raw_CHI_df.time = raw_CHI_df.time.astype(float)

	# 	#Calculate the following statistics and add to the DataFrame
	# 	raw_CHI_df['AVG']=raw_CHI_df[['1','2','3','4','5','6','7','8']].mean(axis=1)
	# 	raw_CHI_df['STD']=raw_CHI_df[['1','2','3','4','5','6','7','8']].std(axis=1)
	# 	raw_CHI_df['RSD']=(raw_CHI_df['STD']/raw_CHI_df['AVG'])*100
	# 	raw_CHI_df['RSD']=(raw_CHI_df['STD']/raw_CHI_df['AVG'])*100

	# 	return raw_CHI_df
	def import_CHI_file(filepath, delimeter = ','):
		possible_col_headers = ['time','Time','potential','Potential','current','Current','charge','Charge']

		
		#Find the column header line
		text_file = open(filepath, 'r')
		lines_reversed = reversed(text_file.readlines())	
		line_num_reversed = 0
		is_first_instance = False			
		for line in lines_reversed:
			if line not in ['\n', '\r\n']:
				line_num_reversed += 1
			if is_first_instance == False and any((header in line) for header in possible_col_headers):
				data_header_line_num_reversed = line_num_reversed
				is_first_instance = True
		text_file.close()

		#Find the total number of lines in file
		data_header_line_num = line_num_reversed - data_header_line_num_reversed	
        
		#Read into a Pandas DF
		raw_CHI_df = pd.read_csv(filepath,sep=delimeter,header=data_header_line_num, parse_dates=[0])	
		raw_CHI_df['Time/s']=raw_CHI_df['Time/s'].astype(float)

		raw_CHI_df['AVG'] = raw_CHI_df.loc[:,' i1/A':].mean(axis=1)
		raw_CHI_df['STD'] = raw_CHI_df.loc[:,' i1/A':].std(axis=1)
		raw_CHI_df['RSD']=(raw_CHI_df['STD']/raw_CHI_df['AVG'])*100
		raw_CHI_df.rename(columns={' i1/A':1,' i2/A':2,' i3/A':3,' i4/A':4,' i5/A':5,' i6/A':6,' i7/A':7,' i8/A':8}, inplace=True)


		return raw_CHI_df

# 		raw_CHI_df.time = raw_CHI_df.time.astype(float)


	#Used to create a dataframe of concentration steps, corresponding time points and average electrode current
	def calibration_curve_df(raw_CHI_df, time_list, conc_list):
		# Make a new dataframes using rows that match the user inputed time steps
		time_df = raw_CHI_df[raw_CHI_df['time'].isin(time_list)]
		time_conc_df = time_df[['time','AVG','STD']]
		time_conc_df['conc'] = conc_list.values

		return time_conc_df

	#Used to iterate through CHI .txt files in a given folder and create a dictionary of time, AVG and conc dataframes
	def create_analysis_dict(path, time_list, conc_list):
		global raw_CHI_dict
		raw_CHI_dict = {}
		global analysis_dict 
		analysis_dict = {}

		for file in os.listdir(path):
			if file.endswith(".txt"):
				name = os.path.splitext(file)[0]
				# print(name)
				# print(os.path.join(path,file))
				raw_CHI_dict[name] = calibration_analytics.import_CHI_file(filepath = (os.path.join(path,file)))
				analysis_dict[name] = calibration_analytics.calibration_curve_df(raw_CHI_df=raw_CHI_dict[name],time_list=time_list, conc_list=conc_list)
		return analysis_dict
		return raw_CHI_dict

	def create_raw_CHI_dict(path):
		raw_CHI_dict = {}
		for file in os.listdir(path):
			if file.endswith(".txt"):
				name = os.path.splitext(file)[0]
				raw_CHI_dict[name] = calibration_analytics.import_CHI_file(filepath = (os.path.join(path,file)))

		return raw_CHI_dict

	def create_calibration_dict(path, time_list, conc_list):

		calibration_dict = {}

		for file in os.listdir(path):
			if file.endswith(".txt"):
				name = os.path.splitext(file)[0]
				calibration_dict[name] = calibration_analytics.calibration_curve_df(raw_CHI_df=raw_CHI_dict[name],time_list=time_list, conc_list=conc_list)
		return calibration_dict


	def calc_polyfit_parameters(calibration_dict):
		polyfit_linear_parameters = {}
		sensor_list = []
		for key in calibration_dict:
			num_x = pd.to_numeric((calibration_dict[key]['conc']))
			num_y = pd.to_numeric((calibration_dict[key]['AVG']))
			p2 = np.polyfit(num_x,num_y,2)
			polyfit_linear_parameters.update({key:p2[1]})


		return polyfit_linear_parameters

	def calc_LOD(calibration_dict, time_list,raw_CHI_dict):
		zero_current_electrodes_stdev_list = {}
		polyfit_linear_parameters = calibration_analytics.calc_polyfit_parameters(calibration_dict)
		for key in raw_CHI_dict:
			zero_current=raw_CHI_dict[key].iloc[0:int(time_list[0])]
			zero_current_electrodes = zero_current.iloc[:,1:9]
			zero_current_electrodes_stdev = zero_current_electrodes.mean(axis=1).std()
			zero_current_electrodes_stdev_list.update({key:zero_current_electrodes_stdev})
		LOD_dict = {}
		for key in polyfit_linear_parameters:
			LOD = (3.3* zero_current_electrodes_stdev_list[key])/polyfit_linear_parameters[key]
			LOD_dict.update({key:LOD})
		return LOD_dict

	def calc_LOQ(calibration_dict, time_list,raw_CHI_dict):
		zero_current_electrodes_stdev_list = {}
		polyfit_linear_parameters = calibration_analytics.calc_polyfit_parameters(calibration_dict)
		for key in raw_CHI_dict:
			zero_current=raw_CHI_dict[key].iloc[0:int(time_list[0])]
			zero_current_electrodes = zero_current.iloc[:,1:9]
			zero_current_electrodes_stdev = zero_current_electrodes.mean(axis=1).std()
			zero_current_electrodes_stdev_list.update({key:zero_current_electrodes_stdev})
		LOQ_dict = {}
		for key in polyfit_linear_parameters:
			LOQ = (10* zero_current_electrodes_stdev_list[key])/polyfit_linear_parameters[key]
			LOQ_dict.update({key:LOQ})
		return LOQ_dict

	def drop_outliers(raw_calibration_df):
	    #Function to return the magnitude of a float
	    def magnitude(x):
	        return int(math.floor(math.log10(x)))
	    #Import only the electrodes
	    imported_df_electrodes=raw_calibration_df.drop(['Time/s','AVG','STD','RSD'], axis=1)
	    #Make a list of the standard deviation of each electrode
	    std_list = []
	    for column in imported_df_electrodes:
	#         print(magnitude(imported_df_electrodes[column].std()))
	        std_list.append(abs(magnitude(imported_df_electrodes[column].std())))
	    std_list=pd.Series(std_list)
	    #Take the mean of the list of electrode standard deviations. 
	    #Drop a column if its SD is more than 1.75 standard deviations than the mean of the list of electrode SDs
	    for i in enumerate(std_list):
	        if i[1] > (1.75*std_list.std())+std_list.mean():
	            std_list_dropped = std_list.loc[std_list.values != i[1] ]
	        elif i[1] > (1.75*std_list.std())+std_list.mean():
	            std_list_dropped = std_list.loc[std_list.values != i[1] ]
	   #Create a list of strings corresponding to the electrodes to drop
	    drop_list= list(std_list_dropped.index)
	    drop_list =[(int(item)+1) for item in drop_list ]
	    drop_list_int=drop_list
	    drop_list = [str(item) for item in drop_list]
	    drop_list=list(map(int,drop_list))

	    #Compare dropped and normal lists
	    regular_list = list(std_list.index + 1)
	    print ("Column {} was dropped".format(list(set(regular_list).difference(set(drop_list_int)))))
	    dropped_column = list(set(regular_list).difference(set(drop_list_int)))
	    #Return the electrode # that was dropped and the new DF, wherein that electrode has been dropped
	    cleaned_df = raw_calibration_df[drop_list]
	    cleaned_df['AVG'] = cleaned_df.mean(axis=1)
	    cleaned_df['STD'] = cleaned_df.std(axis=1)
	    cleaned_df['RSD'] = (cleaned_df['STD']/cleaned_df['AVG'])*100
	    cleaned_df['Time/s']=raw_calibration_df['Time/s']
	    
	    return dropped_column,cleaned_df




    



    

	



	 





			



# #Test parameters
# time_list= ['600', '1200', '1800', '2400', '3000']
# conc_list= ['5', '10', '15', '20', '25']
# conc_list = pd.Series(conc_list)

# # #Test to iterate through a folder contents and run both methods
# path ='/Users/adityamookerjee/Documents/Code/Calibration Analysis/Sample Data' 
# print(calibration_analytics.create_analysis_dict(path=path, time_list=time_list, conc_list=conc_list)
# )


